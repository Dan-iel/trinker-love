const people = require('./people');

module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    allMale: function(p){
        //return people.filter(function(person) { return person.gender == "Male" })
        return people.filter(allboys =>  allboys.gender == "Male")
    },
    
    //result = people.filter(person =>  person.gender == "Male")
    /*function(p){
        return "not implemented".red;
    }*/

    allFemale: function(p){
        return people.filter(allgirls =>  allgirls.gender == "Female")
    },

    nbOfMale: function(p){
        return this.allMale(p).length;
    },

    nbOfFemale: function(p){
        return this.allFemale(p).length;
    },

    allmanlike: function(p){
        return people.filter(allgirls =>  allgirls.looking_for == "M")
    },

    /*allladylike: function(p){
        return people.filter(allgirls =>  allgirls.looking_for == "F")
    },*/

    nbOfMaleInterest: function(p){
        return this.allmanlike(p).length;
    },

    nbOfFemaleInterest: function(p){
        return (p.filter(allgirls =>  (allgirls.looking_for == "F"))).length;
    },

    nbOfPersonp2000: function(p){
        return p.filter(person =>  parseInt(person.income.substring(1)) > 2000).length;
    },

    nbOfDrama: function(p){
        return p.filter(person =>  person.pref_movie.includes("Drama")).length;
    },

    nbOfFema: function(p){
        return p.filter(person =>  (person.pref_movie.includes("Sci-Fi") && person.gender == "Female")).length;
    },

    nbOfDoc1482: function(p){
        return p.filter(person =>  (person.pref_movie.includes("Documentary") && parseInt(person.income.substring(1)) > 1482)).length;
    },

    nbOfMore4000: function(p){
        const richPeople = p.filter(person => (parseFloat(person.income.substring(1)) > 4000)) 
        let list = [];
        richPeople.forEach(person => {
            //list.push(person.first_name + "  " + person.last_name + "  " + person.id + "  " + person.income);
            list.push(`First name is:  ${person.first_name} Last name is:  ${person.last_name} Id is:  ${person.id} Income is: ${person.income}`);
        });
        return list;
    },

    hommeplusRiche: function(p){
        let max = 0;
        let rich;
        this.allMale(p).forEach(person => {
            let income = parseFloat(person.income.substring(1));
            if(income > max) {
                max = income;
                rich = person; 
            }
        }); 
        return max + ' ' + rich.id + ' ' + rich.first_name + " " + rich.last_name;
    },

    SalaireMoyen: function(p){
        let somme = 0;
        
        
            for(let i = 0; i < p.length; i++) {
                let income = parseFloat(p[i].income.substring(1));
                somme += income;
            }
        return (somme / p.length).toFixed(2);
    },

    SalaireMedian: function(p) {
       const tablemedian = p.sort((a, b) => parseFloat(b.income.substring(1)) - parseFloat(a.income.substring(1)));
       let mean;
       if(p.length % 2 == 0){
           let income1 = parseFloat(p[p.length / 2].income.substring(1))
           let income2 = parseFloat(p[(p.length / 2) - 1].income.substring(1))
            mean = (income1 + income2) / 2;
       } else {
           mean = parseFloat(p[p.length / 2].income.substring(1));
       }
       return mean + " €";
    },

    nbOfNorthHem: function (p) {
        const nodhes = p.filter(person => (person.latitude) > 0);
        //let nodheslist = [];
       // nodhes.forEach(person => {
            //list.push(person.first_name + "  " + person.last_name + "  " + person.id + "  " + person.income);
           // nodhes.push(/*`First name is:  ${person.first_name} Last name is:  ${person.last_name} Id is:  ${person.id} Income is: ${person.income}`*/);
       // });
        return nodhes.length;

    },

    aveSudHem: function (p) {
        const sudhes = p.filter(person => (person.latitude) < 0);
        return this.SalaireMoyen(sudhes) + " €";
    },

    voisinBenCawt: function (p){
        Cawt = p.filter(cawt =>  cawt.last_name == "Cawt");
        x = p.filter(cawt =>  cawt.last_name !== "Cawt");
        //console.log(Cawt[0].longitude);
        //console.log(x)

         function toRad(Value) {
            /** Converts numeric degrees to radians */
            return Value * Math.PI / 180;
        }
        
        function haversine(lat1,lat2,lng1,lng2){
            rad = 6372.8; // for km Use 3961 for miles
            deltaLat = toRad(lat2-lat1);
            deltaLng = toRad(lng2-lng1);
            lat1 = toRad(lat1);
            lat2 = toRad(lat2);
            a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) + Math.sin(deltaLng/2) * Math.sin(deltaLng/2) * Math.cos(lat1) * Math.cos(lat2); 
            c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
            return  rad * c;

        }
/*
perpigen 42.6990086 ,2.8344715
montpellier  43.6100109, 3.8391499
        console.log(haversine(42.6990086,43.6100109,2.8344715,3.8391499));*/

        function calculate(){
            let per;
            let result = haversine(Cawt[0].latitude,((Cawt[0].latitude) + 90),Cawt[0].longitude,((Cawt[0].longitude) + 90));
                for (let i=0;i<x.length;i++){ 
                let ans = haversine(Cawt[0].latitude,x[i].latitude,Cawt[0].longitude,x[i].longitude);
                if (ans < result){//nearest 
                    result = ans;
                    per = x[i];
                    
                }       
            }
            return `"Distance entree eux " ${result.toFixed(2)}, ${per.first_name} ${per.last_name} ${per.id}` ;
        }

        return calculate()
    },

    voisinRuiBrach: function (p){
        Brach = p.filter(brach =>  brach.last_name == "Brach");
        x = p.filter(brach =>  brach.last_name !== "Brach")

         function toRad(Value) {
            /** Converts numeric degrees to radians */
            return Value * Math.PI / 180;
        }
        
        function haversine(lat1,lat2,lng1,lng2){
            rad = 6372.8; // for km Use 3961 for miles
            deltaLat = toRad(lat2-lat1);
            deltaLng = toRad(lng2-lng1);
            lat1 = toRad(lat1);
            lat2 = toRad(lat2);
            a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) + Math.sin(deltaLng/2) * Math.sin(deltaLng/2) * Math.cos(lat1) * Math.cos(lat2); 
            c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
            return  rad * c;

        }

        function calculate(){
            let per;
            let result = haversine(Brach[0].latitude,((Brach[0].latitude) + 90),Brach[0].longitude,((Brach[0].longitude) + 90));
                for (let i=0;i<x.length;i++){ 
                let ans = haversine(Brach[0].latitude,x[i].latitude,Brach[0].longitude,x[i].longitude);
                if (ans < result){//nearest 
                    result = ans;
                    per = x[i];
                    
                }       
            }
            return `"Distance entree eux " ${result.toFixed(2)}, ${per.first_name} ${per.last_name} ${per.id}` ;
        }

        return calculate()
    },

    les10VoisinsJoséeBoshard: function (p){
        Boshard = p.filter(boshard =>  boshard.last_name == "Boshard");
        x = p.filter(boshard =>  boshard.last_name !== "Boshard")

         function toRad(Value) {
            /** Converts numeric degrees to radians */
            return Value * Math.PI / 180;
        }
        
        function haversine(lat1,lat2,lng1,lng2){
            rad = 6372.8; // for km Use 3961 for miles
            deltaLat = toRad(lat2-lat1);
            deltaLng = toRad(lng2-lng1);
            lat1 = toRad(lat1);
            lat2 = toRad(lat2);
            a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) + Math.sin(deltaLng/2) * Math.sin(deltaLng/2) * Math.cos(lat1) * Math.cos(lat2); 
            c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
            return  rad * c;

        }

        function calculate(){
            let per;
            let group = [];
            let full = [];
            let result = haversine(Boshard[0].latitude,((Boshard[0].latitude) + 180),Boshard[0].longitude,((Boshard[0].longitude) + 180));
            //console.log(result);
                for (let i=0; i<x.length; i++){ 
                let ans = haversine(Boshard[0].latitude,x[i].latitude,Boshard[0].longitude,x[i].longitude);
                if (ans < result){//nearest 
                    //for(let j = group.length; j < 0; j++){
                    result = ans;
                    full.push(result);
                    //console.log(full);
                    per = x[i];
                    group.push(per);
                    //console.log(group);
                    //}
                }       return group;
            }
            return `"Distance entree eux " ${result.toFixed(2)}, ${group.first_name} ${group.last_name} ${group.id}` ;
        }

        return calculate()
    },


    match: function(p){
        return "not implemented".red;
    }
}//Josée Boshard